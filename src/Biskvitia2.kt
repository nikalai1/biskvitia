
import kotlin.math.pow
import kotlin.math.sqrt

fun main() {

    val point1 = koordinati(x = 4.0, y = 0.0)
    val point2 = koordinati(x = 6.0, y = 6.0)

    println(point1.equals(point2))


    println(point1.SatavisMimartMobruneba())
    println(point2.SatavisMimartMobruneba())
    println(point1.MandziliMatShoris(point2))
}

class koordinati(private val x: Double, private val y: Double): Any() {
    override fun toString(): String {
        return "$x, $y"
    }

    fun SatavisMimartMobruneba(): koordinati {
        return koordinati(x * -1, y * -1)
    }

    fun MandziliMatShoris(other: koordinati): Double {
        val xaxarisxebuli = (x - other.x).pow(2)
        val yaxarisxebuli = (y - other.y).pow(2)
        return sqrt(xaxarisxebuli + yaxarisxebuli)
    }

    /////////////////////////////////////////       მოქმედებები წილადებზე        ///////////////////////////////////////
    fun main() {


        var f1 = Fraction(2.0, 8.0)

        var f2 = Fraction(3.0, 33.0)


        println(f1)
        println(f2)

        println(f1 == f2)

        println(f1.multiply(f2))

        println(f1.add(f2))
    }

    class Fraction(
        val numerator: Double,
        val denominator: Double
    ) {

        override fun toString(): String {
            return "$numerator/$denominator"
        }

        override fun equals(other: Any?): Boolean {
            if (other is Fraction)
                return numerator * other.denominator / other.numerator == denominator
            return false
        }

        fun add(other: Fraction): Fraction {
            val newDenominator = denominator * other.denominator
            val newNumerator1 = newDenominator / denominator * numerator
            val newNumerator2 = newDenominator / other.denominator * other.numerator
            return Fraction(newNumerator1 + newNumerator2, newDenominator)
        }

        fun multiply(other: Fraction): Fraction {
            return Fraction(numerator * other.numerator, denominator * other.denominator)
        }
    }
}